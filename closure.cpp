#include <array>
#include <cstdint>
#include <iostream>
#include <typeinfo>
#include <algorithm>
#include <functional>

using MyArray = std::array<std::uint32_t, 5>;

void PrintArrar(const std::function<void(MyArray::value_type)>& myFunction)
{
    MyArray myArray {1, 2, 3, 4, 5};

    std::for_each(myArray.begin(), myArray.end(), myFunction);
}

int main()
{
    auto myClosure = [](auto&& number) {
        std::cout << number << "\n";
    };

    std::cout << "Name: " << typeid(myClosure).name() << "\n";

    PrintArrar(myClosure);

    return 0;
}