#include <iostream>

int main()
{
    while (1)
    ;
}

void unreachable()
{
    std::cout << "Hello World!\n";
}

/*
Compile with Clang
clang++ -O1 -Wall -o FILE_NAME FILE_NAME.cpp
After compiling
that will show "Hello World!"
 */