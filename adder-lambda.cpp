#include <iostream>
#include <functional>

template<typename T>
auto add_test(const T& a)
{
    auto inner = [&](const T& b) { return a + b; };
    return inner;
}

int main()
{
    int a {5}, b {3};

    // Methond 1 need #include <functional>
    auto adder = [](int x) -> std::function<int(int)> {
        return [=](int y) { return x + y; };
    };
    
    // Method 2
    auto plus = [](auto a) {
        return [=](auto&& b) { return a + b; };
    };

    // Method 3
    auto add  = [](auto i) {
        return [=](auto j) { return i + j; };
    };

    auto add8 = adder(8.0);
    std::cout << "Method 1\n";
    std::cout << "0 plus 8 equal " << add8(0.0) << "\n";
    std::cout << "0   + 8   = " << adder(8)(0) << "\n\n";

    std::cout << "Method 2\n";
    std::cout << "1.1 + 7.7 = " << plus(1.1)(7.7) << "\n";
    std::cout << "5.8 + 3   = " << plus(5.8)(3)   << "\n\n";

    std::cout << "Method 3\n";
    std::cout << "The 3rd method is: " << add(3.3)(5.5) << "\n\n";
    
    std::cout << "Template Way\n";
    std::cout << "The INt   result is: " << add_test(a)(b) << "\n";
    std::cout << "The Float result is: " << add_test(5.5)(3.3) << "\n";
    
    return 0;
}