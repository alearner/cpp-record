#include <iostream>

void swap1(int *x, int * y)
{
    *x = *x + *y;
    *y = *x - *y;
    *x = *x - *y;
}

void swap2(int &x, int &y)
{
    x = x + y;
    y = x - y;
    x = x - y;
}

void swap3(int *x, int *y)
{
    const int temp{*x};
    *x = *y;
    *y = temp;
}

int main()
{
    int a{1}, b{2};
    int *c{&a}, *d{&b};
    int e{5}, f{6};

    std::cout << "Before Swap:\na = " << a << " b = " << b << "\n"
              << "c = " << *c << " d = " << *d << "\n"
              << "e = " << e << " f = " << f << "\n\n";

    swap1(&a, &b);
    std::cout << "Swap1:\na = " << a << " b = " << b << "\n";
    
    swap2(*c, *d);
    std::cout << "Swap2:\nc =" << *c << " d = " << *d << "\n";
    swap2(e, f);
    std::cout << "Swap2:\ne = " << e << " f = " << f << "\n";

    swap3(&e, &f);
    std::cout << "Swap3:\ne = " << e << " f = " << f << "\n";
    swap3(c, d);
    std::cout << "Swap3:\nc = " << *c << " d = " << *d << "\n";

    return 0;
}