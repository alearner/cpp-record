#include <thread>
#include <iostream>

// since from C++11
thread_local int val {0};

void test()
{
    val = 30;
    std::cout << "inside test(): " << val << " " << &val << "\n"; 
}

int main()
{
    val = 15;
    std::cout << "inside main(): " << val << " " << &val << "\n";
    
    std::thread t1(test);
    t1.join();
    
    std::cout << "inside main(): " << val << " " << &val << "\n";

    return 0;
}