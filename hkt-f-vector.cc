#include <iostream>
#include <vector>
#include <functional>

template <template <typename> class F, typename A, typename B>
F<B> fmap(const F<A>& fa, std::function<B(A)> f)
{
    F<B> result;
    for (const auto& a : fa) {
        result.push_back(f(a));
    }
    return result;
}

template <typename A, typename B>
std::vector<B> fmap(const std::vector<A>& fa, std::function<B(A)> f)
{
    std::vector<B> result;
    for (const auto& a : fa) {
        result.push_back(f(a));
    }
    
    return result;
}

template <template <typename> class M, typename A, typename B>
M<B> bind(const M<A>& ma, std::function<M<B>(A)> f)
{
    M<B> result;
    for (const auto& a : ma) {
        auto mb = f(a);
	
        for (const auto& b : mb) {
            result.push_back(b);
        }
    }
    
    return result;
}

template <typename A, typename B>
std::vector<B> bind(const std::vector<A>& ma, std::function<std::vector<B>(A)> f)
{
    std::vector<B> result;
    for (const auto& a : ma) {
        auto mb = f(a);
	
        for (const auto& b : mb) {
            result.push_back(b);
        }
    }
    
    return result;
}

int main()
{
    std::vector<int> vec = {1, 2, 3, 4, 5};
    std::function<int(int)> doubleFunc = [](int x) { return x * 2; };
    std::vector<int> doubledVec = fmap(vec, doubleFunc);

    for (const auto& x : doubledVec) {
        std::cout << x << " ";
    }
    std::cout << "\n";

    std::vector<int> vec2 = {1, 2, 3};
    std::function<std::vector<int>(int)> func = [](int x)
    {
        return std::vector<int>{x, x * 2};
    };

    std::vector<int> result = bind<std::vector, int, int>(vec2, func);
    for (const auto& x : result) {
        std::cout << x << " ";
    }
    std::cout << "\n";

    return 0;
}