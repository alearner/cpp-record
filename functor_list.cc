#include <iostream>
#include <vector>
#include <list>
#include <algorithm>

template <template <typename> class F, typename A>
struct Functor
{
    template <typename Func>
    static auto fmap(Func f, F<A> fa) -> F<decltype(f(*fa.begin()))>
	{
        F<decltype(f(*fa.begin()))> result;
        std::transform(fa.begin(), fa.end(), std::inserter(result, result.end()), f);
        return result;
    }
};

template <typename A>
using List = std::list<A>;

template <typename A>
struct Option
{
    bool is_some;
    A value;
};

template <typename A>
struct FunctorInstance : Functor<List, A> {};

int main()
{
    List<int> lst = {1, 2, 3, 4, 5};
    auto squared = FunctorInstance<int>::fmap([](int x) { return x * x; }, lst);

    std::cout << "Squared list: ";
    for (const auto& x : squared) {
        std::cout << x << " ";
    }
    std::cout << std::endl;

    return 0;
}