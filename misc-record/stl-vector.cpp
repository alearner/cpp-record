
#include <vector>
#include <iostream>
#include <algorithm>

int main()
{
    std::vector<int> odd  {1, 3, 5, 7, 9};
    std::vector<int> even {0, 2, 4, 6, 8};
    
    std::cout << "Normal:\nOdd:\n    ";
    // also can use `auto` with it
    for (std::vector<int>::iterator it = odd.begin(); it < odd.end(); ++it) {
        std::cout << *it << " ";
    }
    std::cout << "\nEven:\n    ";
    
    for (std::vector<int>::iterator it = even.begin(); it < even.end(); ++it) {
        std::cout << *it << " ";
    }
    std::cout << "\n\n";
    
    std::cout << "Reverse:\nOdd:\n    ";
    std::for_each(odd.rbegin(), odd.rend(), [](const int n) { std::cout << n << " "; });
    std::cout << "\nEven:\n    ";
    
    std::for_each(even.rbegin(), even.rend(), [](const int n) { std::cout << n << " ";  });
    std::cout << "\n\n";
    
    std::cout << "Vector Size:     " << odd.size()     << "\n";
    std::cout << "Vector Max_Size: " << odd.max_size() << "\n";
    std::cout << "Vector Capacity: " << odd.capacity() << "\n";
    
    std::cout << "First  Element:  " << odd.front() << "\n";
    std::cout << "Second Element:  " << odd.back()  << "\n";
    
    return 0;
}