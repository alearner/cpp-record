#include <iostream>
#include <string>
#include <string_view>

int main()
{
    std::cout << "Hello, World!\n"; // C-styel string literal
    std::cout << "Hello " "C++\n";  // C++ concatenate sequential string literal

    using namespace std::literals;

    std::cout << "C-style literal\n";
    std::cout << "C++ style literal\n"s;
    std::cout << "C++ style with string_view\n"sv;

    return 0;
}