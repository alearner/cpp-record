#include <iostream>

int main()
{
    int a{ 5 };
    int b{ 6 };
    int *ptr = &a;
    int &ref = b;

    std::cout << "a   = " << a   << "   &a   = " << &a   << "\n\n";
    std::cout << "ptr = " << ptr << "   *ptr = " << *ptr << "\n\n";
    std::cout << "b   = " << b   << "   &b   = " << &b   << "\n\n";
    std::cout << "ref = " << ref << "   &ref = " << &ref << "\n";

    std::cout << "\nIndirection to a Reference.\n*ref = " << *(&ref) << "\n";

    return 0;  
}