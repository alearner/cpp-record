#include <iostream>

class Point
{
private:
    double x, y;

public:
    Point() : x {0.0}, y {0.0} {}

    Point(double a, double b) : x {a}, y{b} {}

    void show() const
    {
        std::cout << x << " " << y << "\n";
    }

    Point operator +(const Point& p)
    {
        Point res;
        res.x = x + p.x;
        res.y = y + p.y;
        return res;
    }
};

int main()
{
    Point p1(5.0, 5.0);
    Point p2(10.0, 20.0);
    Point p3 {p1 + p2};

    p1.show();
    p2.show();
    p3.show();

    return 0;
}

/*
CAN NOT Overloading:
    . :: .* ?: sizeof etc..
 */