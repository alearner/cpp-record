#include <string>
#include <iostream>

template <typename T>
T large(T x, T y)
{
    return x > y ? x : y;
}

int main()
{
    std::string str_a {"A"};
    std::string str_l {"L"};
    
    std::string strO {"O"};
    std::string stro {"o"};

    int    min {5},     max {10};
    double dmin {19.0}, dmax {86.0};
    
    std::cout << "The Large of 'A' and 'L' is:  "  << large(str_a, str_l) << "\n";
    std::cout << "The Large of 'O' and 'o' is:  "  << large(strO, stro)   << "\n";

    std::cout << "The Large of 5 and 10 is:      " << large(min, max)     << "\n";
    std::cout << "The large of 19.0 and 86.0 is: " << large(dmin, dmax)   << "\n";
    
    return 0;
}