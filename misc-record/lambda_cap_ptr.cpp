#include <vector>
#include <iostream>
#include <functional>

int main()
{
    auto ptr = std::make_unique<int>(42);
    std::vector<int> vec {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    std::transform(vec.begin(), vec.end(), vec.begin(),
        [ptr = std::move(ptr)](auto x) {
            return x + *ptr;
    });
    
    for (auto v : vec) {
        std::cout << v  << " ";
    }
    std::cout << "\n";
        
    return 0;
}