#include <iostream>

class StaticMember
{
public:
    static int value;         // Decalaration
};

int StaticMember::value = 10; // Definition

/*
  For a few simple special cases, it is possible to initialize a static member in the class declaration. 
  The static member must be a const of an integral or enumeration type, or a constexpr of a literal type,
  and the initializer must be a constant-expression.
 */

class Example
{
public:
    static const int c1 = 5;
    static constexpr float c5 = 10.0;
};

int main()
{
    std::cout << "Example :: c1 = " << Example::c1 << "\n";
    const int *ptr = &Example::c1;
    std::cout << "*ptr = " << *ptr << "\n";
    std::cout << "StaicMemver :: value = " << StaticMember::value << "\n";
    std::cout << "Example :: c5 = " << Example::c5 << "\n";
    
    return 0;
}