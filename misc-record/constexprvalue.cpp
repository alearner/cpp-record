#include <iostream>

int main()
{
    int                  i  {5};
    const int            j  {6};
    static const int     k  {7};
    static const int     k1 {8};
    constexpr int        l  {9};
    static constexpr int l1 {10};
  
    std::cout << "value of    i = " <<   i << "\n";
    std::cout << "address of  i = " <<  &i << "\n";
    std::cout << "value of    j = " <<   j << "\n";
    std::cout << "address of  j = " <<  &j << "\n";
    std::cout << "value of    k = " <<   k << "\n";
    std::cout << "address of  k = " <<  &k << "\n";
    std::cout << "value of   k1 = " <<  k1 << "\n";
    std::cout << "address of k1 = " << &k1 << "\n";
    std::cout << "value of    l = " <<   l << "\n";
    std::cout << "address of  l = " <<  &l << "\n";
    std::cout << "value of   l1 = " <<  l1 << "\n";
    std::cout << "address of l1 = " << &l1 << "\n";
    
    return 0; 
}