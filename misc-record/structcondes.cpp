#include <iostream>

struct Actor
{
    Actor();
    void DoJob();
    ~Actor();
};

Actor::Actor()
{
    std::cout << "Start\n";
}

void Actor::DoJob()
{
    std::cout << "Working\n";
}

Actor::~Actor()
{
    std::cout << "End\n";
}

int main()
{
    Actor act;
    for (int i = 1; i < 11; ++i) {
        act.DoJob();        
    }
    
    return 0; 
}