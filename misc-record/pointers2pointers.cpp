#include <iostream>

int main()
{
    int i   {5};
    int *j  {&i};
    int **k {&j};
    
    std::cout << i << " " << &i << "\n"
              << *j <<  " " << j << " " << &j << "\n"
              << **k << " " << k << " " << &k << " " << *k << "\n";
    return 0;
}