#include <iostream>

class A
{
public:
    int v {};
    
    virtual int& operator=(int x)
	{
	    v = x;
	    return v;
	};
};

class B : public A
{
public:
    int v1 {};
    
    int& operator=(int x)
	{
	    v1 = x;
	    return v1;
	};
};

int main()
{
    int x {}, y{};
    A a;
    B b;
    (0 ? a : b) = 3;
    std::cout << a.v << "\n" << b.v << "\n";
    std::cout << b.v1 << "\n";
    
    std::cout << "----------\n";
    
    (1 ? x : y) = 3;
    std::cout << x << "\n" << y << "\n";
    return 0;
}