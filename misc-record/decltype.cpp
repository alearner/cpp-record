#include <iostream>

template<typename I, typename J>
auto add(I i, J j) -> decltype(i + j)
{
  return i + j;
}

auto main() -> int
{
	std::cout << "[decltype]\n";
    
    auto d = add<int, double>(5, 3.8);
    std::cout << "result of 5 + 3.8 = " << d  << "\n";
    
    return 0;
}