#include <iostream>
#include <type_traits>

template <typename T>
auto GetValue(T t)
{
    if constexpr (std::is_pointer<T>::value) {
        return *t;
    } else {
        return t;
    }
}

int main()
{
    int v {10};
    int *ptr {&v};
    std::cout << "Value    : " << GetValue(v)  << "\n";
    std::cout << "Reference: " << GetValue(&v) << "\n";
    std::cout << "Value    : " << GetValue(ptr) << "\n";
    std::cout << "'Value   : " << GetValue(*ptr) << "\n";
    std::cout << "Reference: " << GetValue(&ptr) << "\n";
    return 0;
}