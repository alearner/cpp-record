#include <vector>
#include <iostream>
#include <algorithm>

int main()
{
	std::cout << "[lambda_capturing_by_reference]\n";
    
    std::vector<int> vec{};
    
    for (int i = 0; i < 10; ++i) {
    	vec.push_back(i);
    }
    
    std::cout << "Original Data:\n";
    
    std::for_each (std::begin(vec), std::end(vec), [](int n) { std::cout << n << " "; });
    std::cout << "\n";
    
    /*
	std::for_each(vec.begin(), vec.end(), [](int n) { std::cout << n << " "; });
    std::cout << "\n";
    */
    
    int a = {666};
    int b = {888};
    
    /* Capturing by value  with mutalbe
    std::for_each(std::begin(vec), std::end(vec), [=](int& x) mutable { const int old = x; 
    */
    std::for_each(std::begin(vec), std::end(vec), [&a, &b](int& x) { const int old = x;
    															x *= 2; 
    															a = b; 
    															b = old; });
    
    std::cout << "Squared Data:\n";
    
    std::for_each(std::begin(vec), std::end(vec), [](int n) { std::cout << n << " "; });
    std::cout << "\n";
    
    std::cout << "a = " << a << "\n";
    std::cout << "b = " << b << "\n";

	return 0;
}