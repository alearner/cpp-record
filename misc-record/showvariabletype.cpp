#include <iostream>
#include <typeinfo>

int main()
{
    auto var {1};
    std::cout << var << "\n";
    std::cout << "var is a " << typeid(var).name() << " type.\n";
    return 0;
}

/*
   Run with: ./ProgramName | c++filt -t
 */