#include <iostream>

class A
{
private:
    int m_y;

protected:
    int p_z;

public:
    int x;
    
    A() : x {0}, m_y {1}, p_z {2} {}
    
    int getX() const
    {
        return x;
    }
    
    int getY() const
    {
        return m_y;
    }
    
    int getZ() const
    {
        return p_z;    
    }
};

class B : public A
{
};


int main()
{
    B b;
    
    std::cout << b.getX() << "\n";
    std::cout << b.getY() << "\n";
    std::cout << b.getZ() << "\n";
    
    return 0;
}