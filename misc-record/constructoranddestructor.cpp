#include <iostream>

class Test
{
private:
    int m_Value;
    
public:
    Test();
    // Test() = default;
    Test(const int);
    Test(Test &t);
    void ShowValue() const;
    ~Test();
};

Test::Test()
{
}

/*
  Test::Test() = default;
  Same as above. But more BETTER THAN above.(For Compiler.)
 */

// Initializer List
Test::Test(const int x) : m_Value{x} {}

// Copy Constructor
Test::Test(Test &t)
{
    m_Value = t.m_Value;
}

void Test::ShowValue() const
{
    std::cout << m_Value << "\n";
}

Test::~Test()
{
    std::cout << "Destructor.\n";
}

int main()
{
    Test t1;
    std::cout << "t1 Done.\n";
    
    Test t2 {10};
    t2.ShowValue();
    std::cout << "t2 Done.\n";
    
    Test t3 {t2};
    t2.ShowValue();
    std::cout << "t3 Done.\n";

    return 0;
}