#include <iostream>

class Base
{
public:
    virtual void say() const
    {
        std::cout << "Base Class Virtual Function.\n";
    }

    // Default Argument Values in Virtual Functions
    virtual int showit(int i = 5) const
    {
        std::cout << i << "\n";
        return i;
    }
    
    virtual ~Base()
    {
        std::cout << "Base Class Destrutor.\n";
    }
};

class Derived : public Base
{
public:
    void say() const override
    {
        std::cout << "Derived Class Virtual Function.\n";
    }

    int showit(int i = 50) const override
    {
        std::cout << i << "\n";
        return i;
    }
};

class Kid : public Base
{
public:
    void say() const override final
    {
        std::cout << "Kid Clsee Virtual Function.\n";
    }

    int showit(int i = 500) const override final
    {
        std::cout << i << "\n";
        return i;
    }

    ~Kid()
    {
        std::cout "Kid Class Destructor\n";
    }
};

int main()
{
    Base    b;
    Derived d;
    Kid     k;

    b.say();
    b.showit();
    d.say();
    d.showit();
    k.say();
    k.showit();
    
    return 0;
}

/*
#include <iostream>

class Base
{
public:
    virtual void Say() const
    {
        std::cout << "Base Class\n";
    }
};

class DerivedA : public Base
{
public:
    void Say() const override final
    {
        std::cout << "Derived A Class\n";
    }
};

class DerivedB : public Base
{
public:
    void Say() const override final
    {
        std::cout << "Derived B Class\n";
    }
};

int main()
{
    Base b;
    DerivedA da;
    DerivedB db;
    
    da.Say();
    b.Say();
    db.Say();
    
    return 0;
}

// Can write override final in diferent derived classes same time. and together derived from BASE CLASS.
 */

/*
 override final or final override it doesnt mater. both right.
 The function in Derived Class are VIRTUAL by default. Can not write.
 The DESTRUCTOR must be VIRTUAL in BASE CLASS.
 */