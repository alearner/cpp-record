#include <vector>
#include <iostream>
#include <algorithm>

int main()
{
    std::cout << "[lambda_capturing_by_value]\n";
    std::vector<int> vec {};

    for (int i = 0; i < 10; ++i) {
        vec.push_back(i);
    }

    std::cout << "Original Data:\n";
    std::for_each(vec.begin(), vec.end(), [](const int n) { std::cout << n << " "; });
    std::cout << "\n";

    int a {6};
    int b {8};

    std::cout << "Between " << a << " and " << b << ":\n";

    //explicitly
    std::for_each(vec.begin(), vec.end(), [a, b](int n) {
        if (n >= a && n <= b) std::cout << n << " "; });
    std::cout << "\n";

    /* implicitly
    std::for_each(vec.begin(), vec.end(), [=](int n) {
        if (n >= a && n <= b) std::cout << n << " "; });
    std::cout << "\n";
    */

    return 0;
}