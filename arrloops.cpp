#include <iostream>

auto main() -> int
{
    int arr[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

    std::cout << "[normal_for_loop]\n";

    for (unsigned int i = 0; i < sizeof(arr) / sizeof(arr[0]); ++i) {
        std::cout << arr[i] << " ";
    }
    std::cout << "\nEnd of [normal_for_loop]\n\n";

    std::cout << "[non-member_begin_and_end]\n";

    for (auto i = std::begin(arr); i != std::end(arr); ++i) {
        std::cout << *i << " ";
    }
    std::cout << "\nEnd of [non-member_begin_and_end]\n\n";

    std::cout << "[range_based_for_loop]\n";

    for (auto a : arr) {
        std::cout << a << " ";
    }
    std::cout << "[\nEnd of [range_based_for_loop]\n";

    return 0;
}